<?php

use common\models\Event;
use yii\helpers\Html;

/**
 * @var Event $model
 */
?>
<div class="col-lg-8 col-sm-6">
    <img class="card-img-top img-thumbnail" src="http://placehold.it/900x350"
         alt="<?= Html::encode($model->show->picture) ?>">
</div>
<div class="col-lg-4 col-sm-6">
    <h4 class="card-title">
        <?= Html::encode($model->show->name) ?>
    </h4>
    <p class="card-text"><?= Html::encode($model->show->description) ?></p>
</div>