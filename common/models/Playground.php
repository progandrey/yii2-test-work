<?php


namespace common\models;

use yii\db\ActiveRecord;

class Playground extends ActiveRecord
{
    public static function tableName()
    {
        return 'playgrounds';
    }
}
